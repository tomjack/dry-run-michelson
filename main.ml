open Tezos_protocol_006_PsCARTHA
open Tezos_protocol_006_PsCARTHA_parameters
open Tezos_client_006_PsCARTHA
open Protocol.Environment.Error_monad

let protocol_init () =
  let ctxt = Memory_context.empty in
  let parameters =
    let open Default_parameters in
    Data_encoding.Binary.to_bytes_exn Data_encoding.json
      (json_of_parameters (parameters_of_constants constants_mainnet)) in
  Environment_context.Context.set ctxt ["version"] (Bytes.of_string "genesis") >>= fun ctxt ->
  Environment_context.Context.set ctxt ["protocol_parameters"] parameters >>= fun ctxt ->
  let genesis_hash =
    Protocol.Environment.Block_hash.of_b58check_exn "BLockGenesisGenesisGenesisGenesisGenesisCCCCCeZiLHU" in
  let header : Tezos_base.Block_header.shell_header = {
    level = 0l ;
    predecessor = genesis_hash ;
    timestamp = Tezos_base.TzPervasives.Time.Protocol.epoch ;
    fitness = Protocol.Fitness_repr.from_int64 0L ;
    operations_hash = Protocol.Environment.Operation_list_list_hash.zero ;
    proto_level = 0 ;
    validation_passes = 0 ;
    context = Protocol.Environment.Context_hash.zero ;
  } in
  Protocol.init ctxt header >>=? fun {context = ctxt; _} ->
  let timestamp = Protocol.Environment.Time.of_seconds (Int64.of_float (Unix.time ())) in
  let protocol_data : Protocol.block_header_data = {
    contents = {
      priority = 0 ;
      proof_of_work_nonce = Bytes.create 0 ;
      seed_nonce_hash = None ;
    } ;
    signature = Protocol.Environment.Signature.zero ;
  } in
  Protocol.begin_construction
    ~chain_id:Protocol.Environment.Chain_id.zero
    ~predecessor_context:ctxt
    ~predecessor_timestamp:header.timestamp
    ~predecessor_fitness:header.fitness
    ~predecessor_level:header.level
    ~predecessor:genesis_hash
    ~timestamp
    ~protocol_data
    ()

let dry_run code parameter entrypoint storage =
  protocol_init () >>=? fun {ctxt; _} ->
  Lwt.return (Protocol.Alpha_context.Contract.of_b58check "tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU") >>=? fun source ->
  Lwt.return (Protocol.Alpha_context.Contract.of_b58check "tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU") >>=? fun sender ->
  Lwt.return (Protocol.Alpha_context.Contract.of_b58check "tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU") >>=? fun self ->
  Protocol.Script_interpreter.execute
    ctxt
    Readable (* TODO ? *)
    {
      source = sender ;
      payer = source ;
      self = self ;
      amount = Protocol.Alpha_context.Tez.zero ;
      chain_id = Protocol.Environment.Chain_id.zero ;
    }
    ~script:{
      code = Protocol.Script_repr.lazy_expr code ;
      storage = Protocol.Script_repr.lazy_expr storage ;
    }
    ~entrypoint
    ~parameter

let () =
  let contract_source = Array.get Sys.argv 1 in
  let parameter_source = Array.get Sys.argv 2 in
  let entrypoint = Array.get Sys.argv 3 in
  let storage_source = Array.get Sys.argv 4 in

  let (contract, errs) =
    Michelson_v1_parser.parse_toplevel
      ~check:false
      contract_source in
  match errs with
  | _ :: _ -> Stdlib.failwith "parse error in contract"
  | [] -> () ;

  let (parameter, errs) =
    Michelson_v1_parser.parse_expression
      ~check:false
      parameter_source in
  match errs with
  | _ :: _ -> Stdlib.failwith "parse error in parameter"
  | [] -> () ;

  let (storage, errs) =
    Michelson_v1_parser.parse_expression
      ~check:false
      storage_source in
  match errs with
  | _ :: _ -> Stdlib.failwith "parse error in storage"
  | [] -> () ;

  match Lwt_main.run (dry_run contract.expanded parameter.expanded entrypoint storage.expanded) with
  | Ok _result -> Format.printf "ok!\n"
  | Error _errs -> Format.eprintf "errors!\n"; exit 1
